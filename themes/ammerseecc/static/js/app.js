
// NAVBAR //
const navSlide = () => {
    const burger = document.querySelector('.burger');
    const nav = document.querySelector('.nav-links');
    const navLinks = document.querySelectorAll('.nav-links li');
    burger.addEventListener('click', () => {
        // Toggle Nav
        nav.classList.toggle('nav-active');
        // Animated Links
        navLinks.forEach((link, index) => {
            if (link.style.animation) {
                link.style.animation = ''
            } else {
                link.style.animation = `navLinkFade 0.5s ease forwards ${index / 7 + 0.37}s`;
            }
        });

        // Burger Slide
        burger.classList.toggle('toggle');
        burger.style.animation = `fade 0.5s ease forwards 0s`;
    });
};

alert("asdf");

$(window).scroll(function () {
    var nav = $('nav');
    var top = 20;
    if ($(window).scrollTop() > top) {
        nav.addClass('fixed');

    } else {
        nav.removeClass('fixed');
    }
});

navSlide();
